#
# 2D Matrices
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2022 Leandro Motta Barros
#

extends SceneTree

var Matrix
var Assert

func _init():
	Matrix = preload("res://Matrix.gd")
	Assert = preload("res://Assert.gd")

	TestGetSet()
	TestInitialization()
	TestInternals()

	quit()


# Tests setting and getting matrix elements.
func TestGetSet() -> void:
	var m = Matrix.make(3, 5)

	m[0][0] = 111
	Assert.isEqual(111, m[0][0])

	m[0][3] = 222
	Assert.isEqual(111, m[0][0])
	Assert.isEqual(222, m[0][3])

	m[2][4] = 333
	Assert.isEqual(111, m[0][0])
	Assert.isEqual(222, m[0][3])
	Assert.isEqual(333, m[2][4])

	m[1][2] = 444
	Assert.isEqual(111, m[0][0])
	Assert.isEqual(222, m[0][3])
	Assert.isEqual(333, m[2][4])
	Assert.isEqual(444, m[1][2])

	m[0][3] = "something else"
	Assert.isEqual(111, m[0][0])
	Assert.isEqual("something else", m[0][3])
	Assert.isEqual(333, m[2][4])
	Assert.isEqual(444, m[1][2])


# Tests matrix initialization.
func TestInitialization() -> void:
	# By default, initializes everything to null
	var m = Matrix.make(22, 33)
	for x in range(22):
		for y in range(33):
			Assert.isNull(m[x][y])

	m = Matrix.make(33, 22, 171)
	for x in range(33):
		for y in range(22):
			Assert.isEqual(171, m[x][y])

	# m = Matrix.make(111,1, "x")
	# for x in range(111):
	# 	for y in range(1):
	# 		Assert.isEqual("x", m[x][y])


# Checks if the internal storage is really column-major.
func TestInternals() -> void:
	var m = Matrix.make(4, 2)
	m[0][0] = 0
	m[0][1] = 1
	m[1][0] = 2
	m[1][1] = 3
	m[2][0] = 4
	m[2][1] = 5
	m[3][0] = 6
	m[3][1] = 7

	# m looks like this:
	#   0 2 4 6
	#   1 3 5 7

	Assert.isEqual([[0, 1], [2, 3], [4, 5], [6, 7]], m)
