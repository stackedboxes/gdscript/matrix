# 2D Matrices

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

A helper to create 2D matrices.
