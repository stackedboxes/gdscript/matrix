#
# 2D Matrices
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2022 Leandro Motta Barros
#

extends Node
class_name SbxsMatrix


# Makes and returns a matrix. The matrix is just an Array of Arrays, with each
# of these "sub-arrays" storing one column. (So, sort of column-major, so that I
# can hope for better cache use when looping my favorite way (i.e., two nested
# loops, first over x/columns, then over y/rows).
static func make(cols: int, rows: int, value = null) -> Array:
	assert(cols > 0)
	assert(rows > 0)

	var m: Array = [ ]
	m.resize(cols)
	for x in range(cols):
		m[x] = [ ]
		m[x].resize(rows)
		if value != null:
			for y in range(rows):
				m[x][y] = value

	return m
